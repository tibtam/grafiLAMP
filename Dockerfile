#from ubuntu 16.04 https://hub.docker.com/_/ubuntu/
FROM ubuntu:latest 
MAINTAINER TeTe <tete@tete.hu>

COPY install-lamp.sh /usr/sbin/install-lamp
RUN install-lamp

# generate our ssl key
COPY setupApacheSSLKey.sh /usr/sbin/setup-apache-ssl-key
ENV SUBJECT /C=US/ST=CA/L=CITY/O=ORGANIZATION/OU=UNIT/CN=localhost
ENV DO_SSL_LETS_ENCRYPT_FETCH false
ENV USE_EXISTING_LETS_ENCRYPT false
ENV EMAIL info@grafibit.hu
ENV DO_SSL_SELF_GENERATION true
RUN setup-apache-ssl-key
ENV DO_SSL_SELF_GENERATION false
ENV CURLOPT_CAINFO /etc/ssl/certs/ca-certificates.crt

# here are the ports that various things in this container are listening on
# for http (apache, only if APACHE_DISABLE_PORT_80 = false)
EXPOSE 80
# for https (apache)
EXPOSE 443
# for MySQL server (mariadb, only if START_MYSQL = true)
EXPOSE 3306
#SSH
EXPOSE 22

# start servers
COPY startServers.sh /usr/sbin/start-servers
ENV START_APACHE true
ENV APACHE_ENABLE_PORT_80 true
ENV START_MYSQL true
ENV ENABLE_DAV false
ENV ENABLE_CRON true
CMD start-servers; sleep infinity

COPY info.php /var/www/html
COPY apacheVirtualHosts.conf /etc/apache2/sites-enabled

# Add volumes for MySQL 
VOLUME  ["/var/lib/mysql"]
VOLUME  ["/var/www/html"]
# Environment variables to configure php
ENV PHP_UPLOAD_MAX_FILESIZE 10M
ENV PHP_POST_MAX_SIZE 10M
ENV PHP_MEMORY_LIMIT 1024M