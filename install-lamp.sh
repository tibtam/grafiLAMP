set -eux pipefail

#apt-get update && apt-get install -y ssh

PHPINI_FOLDER=/etc/php/7.0/apache2/
APACHE_CONF_FILE=/etc/apache2/apache2.conf


# install apache
apt-get update && apt-get install -y apache2
sed -i "s,#ServerName www.example.com:80,ServerName $(hostname --fqdn):80,g" ${APACHE_CONF_FILE}

# enable mod rewrite
sed -i '/^#LoadModule rewrite_module modules\/mod_rewrite.so/s/^#//g' ${APACHE_CONF_FILE}

# solve HTTP TRACE vulnerability: http://www.kb.cert.org/vuls/id/867593
sed -i '$a TraceEnable Off' ${APACHE_CONF_FILE}

# install php
apt-get install -y php libapache2-mod-php
apt-get install -y php-mysqli
apt-get install -y php-mbstring
apt-get install php-xdebug

sed -i 's,#LoadModule deflate_module modules/mod_deflate.so,LoadModule deflate_module modules/mod_deflate.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule expires_module modules/mod_expires.so,LoadModule expires_module modules/mod_expires.so,g' ${APACHE_CONF_FILE}
sed -i 's,LoadModule mpm_event_module modules/mod_mpm_event.so,#LoadModule mpm_event_module modules/mod_mpm_event.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so,LoadModule mpm_prefork_module modules/mod_mpm_prefork.so,g' ${APACHE_CONF_FILE}
sed -i 's,;extension=iconv.so,extension=iconv.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=xmlrpc.so,extension=xmlrpc.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=zip.so,extension=zip.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=bz2.so,extension=bz2.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=curl.so,extension=curl.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=ftp.so,extension=ftp.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=gettext.so,extension=gettext.so,g' ${PHPINI_FOLDER}php.ini

#removing the default virtualhost configuration to allow ours working
rm /etc/apache2/sites-enabled/000-default.conf

# for php-gd
apt-get install -y php-gd
sed -i 's,;extension=gd.so,extension=gd.so,g' ${PHPINI_FOLDER}php.ini

# for php-intl
apt-get install -y php-intl
sed -i 's,;extension=intl.so,extension=intl.so,g' ${PHPINI_FOLDER}php.ini

# for php-mcrypt
apt-get install -y php-mcrypt
sed -i 's,;extension=mcrypt.so,extension=mcrypt.so,g' ${PHPINI_FOLDER}php.ini

# for PHP caching
sed -i 's,;zend_extension=opcache.so,zend_extension=opcache.so,g' ${PHPINI_FOLDER}php.ini

# for exif support
apt-get install -y exiv2
sed -i 's,;extension=exif.so,extension=exif.so,g' ${PHPINI_FOLDER}php.ini

# for sqlite database
#pacman -S --noprogressbar --noconfirm --needed sqlite php-sqlite
#sed -i 's,;extension=sqlite3.so,extension=sqlite3.so,g' /etc/php/php.ini
#sed -i 's,;extension=pdo_sqlite.so,extension=pdo_sqlite.so,g' /etc/php/php.ini

##groupadd -g 89 mysql &>/dev/null
# for mariadb (mysql) database
##useradd -u 89 -g 89 -d /var/lib/mysql -s /bin/false mysql 
# here is a hack to prevent an error during install because of missing systemd
##ln -s /usr/bin/true /usr/bin/systemd-tmpfiles

#echo 'mysql-server mysql-server/root_password password your_password' | debconf-set-selections
#echo 'mysql-server mysql-server/root_password_again password your_password' | debconf-set-selections
apt-get install -y mariadb-server
#rm /usr/bin/systemd-tmpfiles
sed -i 's,;extension=pdo_mysql.so,extension=pdo_mysql.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,;extension=mysql.so,extension=mysql.so,g' ${PHPINI_FOLDER}php.ini
#mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sed -i 's,;extension=mysqli.so,extension=mysqli.so,g' ${PHPINI_FOLDER}php.ini
#sed -i 's,mysql.trace_mode = Off,mysql.trace_mode = On,g' /etc/php/php.ini
#sed -i 's,mysql.default_host =,mysql.default_host = localhost,g' /etc/php/php.ini
#sed -i 's,mysql.default_user =,mysql.default_user = root,g' /etc/php/php.ini

# for dav suppport
sed -i 's,#LoadModule dav_module modules/mod_dav.so,LoadModule dav_module modules/mod_dav.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule dav_fs_module modules/mod_dav_fs.so,LoadModule dav_fs_module modules/mod_dav_fs.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule dav_lock_module modules/mod_dav_lock.so,LoadModule dav_lock_module modules/mod_dav_lock.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule setenvif_module modules/mod_setenvif.so,LoadModule setenvif_module modules/mod_setenvif.so,g' ${APACHE_CONF_FILE}
#sed -i 's,#LoadModule auth_digest_module modules/mod_auth_digest.so,LoadModule auth_digest_module modules/mod_auth_digest.so,g' ${APACHE_CONF_FILE}
#sed -i 's,#LoadModule authn_core_module modules/mod_authn_core.so,LoadModule authn_core_module modules/mod_authn_core.so,g' ${APACHE_CONF_FILE}
#sed -i 's,#LoadModule authn_file_module modules/mod_authn_file.so,LoadModule authn_file_module modules/mod_authn_file.so,g' ${APACHE_CONF_FILE}
#sed -i 's,#LoadModule authz_core_module modules/mod_authz_core.so,LoadModule authz_core_module modules/mod_authz_core.so,g' ${APACHE_CONF_FILE}
#sed -i 's,#LoadModule authz_user_module modules/mod_authz_user.so,LoadModule authz_user_module modules/mod_authz_user.so,g' ${APACHE_CONF_FILE}


# setup ssl
sed -i 's,;extension=openssl.so,extension=openssl.so,g' ${PHPINI_FOLDER}php.ini
sed -i 's,#LoadModule ssl_module modules/mod_ssl.so,LoadModule ssl_module modules/mod_ssl.so,g' ${APACHE_CONF_FILE}
sed -i 's,#LoadModule socache_shmcb_module modules/mod_socache_shmcb.so,LoadModule socache_shmcb_module modules/mod_socache_shmcb.so,g' ${APACHE_CONF_FILE}
sed -i 's,#Include conf/extra/httpd-ssl.conf,Include conf/extra/httpd-ssl.conf,g' ${APACHE_CONF_FILE}
# use Mozilla's recommended ciphersuite (see https://wiki.mozilla.org/Security/Server_Side_TLS):
##sed -i 's/^SSLCipherSuite .*/SSLCipherSuite ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK/g' /etc/httpd/conf/extra/httpd-ssl.conf
# disable super old and vulnerable SSL protocols: SSLv2 and SSLv3 (this breaks IE6 & windows XP)
##sed -i '$a SSLProtocol All -SSLv2 -SSLv3' /etc/httpd/conf/extra/httpd-ssl.conf
# let ServerName be inherited
sed -i '/ServerName teszt.local/d' ${APACHE_CONF_FILE}

apt-get install -y curl
# this is for lets encrypt ssl cert fetching
##apt-get install -y software-properties-common
##add-apt-repository -y ppa:certbot/certbot
##apt-get update & apt-get install -y python-certbot-apache --allow-unauthenticated
##apt-get install -y certbot
apt-get install -y nano

echo "date.timezone = 'Europe/Paris'" >> ${PHPINI_FOLDER}php.ini
echo "Europe/Paris" > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
  
# Add dirs for manage sites (mount from host in run needeed for persistence)
#mkdir /data && mkdir /data/lamp && mkdir /data/lamp/conf && mkdir /data/lamp/www 

chown -R mysql:mysql /var/lib/mysql
  
# Symfony 2 pre requisted
apt-get -y install curl
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# reduce docker layer size
#cleanup-image